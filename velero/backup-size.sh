#!/bin/bash

backup_name="<backup-name>"
total_size=0

# Get the list of items in the backup
items=$(velero backup describe $backup_name --details --json | jq -r '.items[]')

# Iterate over each item and sum up the size
while IFS= read -r item; do
  size=$(echo "$item" | jq -r '.size')
  total_size=$((total_size + size))
done <<< "$items"

# Print the total backup size
echo "Backup Size: $total_size bytes"
